import platform
import re
from globals import DATA_PATH
from globals import MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE
from globals import HOUSE_HOST, HOUSE_PORT, HOUSE_USER, HOUSE_PASSWORD, HOUSE_DATABASE

from DB.db_mysql_driver import MySQLDriver
from DB.db_clickhouse_driver import ClickHouseDriver


def main():
    """
    Создание файлов для передачи в модуль обучения
    """

    # определение БД в зависимости от текущей OS
    osp = platform.platform()
    if osp.startswith('Windows') == 1:
        db_cursor = MySQLDriver(MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE)
    else:
        db_cursor = ClickHouseDriver(HOUSE_HOST, HOUSE_PORT, HOUSE_USER, HOUSE_PASSWORD, HOUSE_DATABASE)

    db_cursor.connect_db()
    # выбор таблицы параллельного корпуса
    texts = db_cursor.get_texts()
    # открытие файлов для записи
    path = DATA_PATH + 'en.txt'
    en = open(path, 'w+', encoding='utf-8')
    path = DATA_PATH + 'ru.txt'
    ru = open(path, 'w+', encoding='utf-8')
    i = 0
    # сохранение корпуса в файлы ru.txt en.txt
    for text in texts:
        # если оба предложения существуют
        if text[2] != '' and text[3] != '':
            i = i + 1
            text[2] = re.sub("^\s+|\n|\r|\s+$", '', text[2])
            en.write(text[2] + '\n')
            text[3] = re.sub("^\s+|\n|\r|\s+$", '', text[3])
            ru.write(text[3] + '\n')
    en.close()
    ru.close()


if __name__ == "__main__":
    main()
