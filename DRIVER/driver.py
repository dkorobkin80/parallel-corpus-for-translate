from abc import ABC

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.support.wait import WebDriverWait


class PatentWebDriver(ABC):
    def __init__(self) -> None:
        """
        Инициализация веб драйвера
        """
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        self._driver = webdriver.Chrome(options=chrome_options,
                                        service=ChromeService(ChromeDriverManager().install()))

    def get_patent_html(self, patent_link: str) -> str:
        """
        Получить html содержимое страницы
        :param patent_link: ссылка на патент
        :return: html страница
        """
        self._driver.get(patent_link)

        wait = WebDriverWait(self._driver, 30)
        return self._driver.page_source

    def quit(self) -> None:
        """
        Закрыть браузер
        :return:
        """
        self._driver.quit()
