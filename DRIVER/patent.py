from typing import List, Dict, Union
from DRIVERS.driver import PatentWebDriver
from bs4 import BeautifulSoup
import re


class GooglePatent(PatentWebDriver):
    def __init__(self, web_driver: PatentWebDriver) -> None:
        """
        конструктор
        :param web_driver: веб драйвер
        """
        self.__soup: BeautifulSoup = None
        self.__web_driver = web_driver
        self.__page = ''

    def load_data(self, patent_link: str) -> None:
        """
        Загрузка страницу патента
        :param patent_link: ссылка на патент
        :return:
        """
        self.__page = self.__web_driver.get_patent_html(patent_link)
        self.__soup = BeautifulSoup(self.__page, 'html.parser')

    def set_data(self, src: str) -> None:
        """
        Передача html страницы в BeautifulSoup
        :param src:
        :return:
        """
        self.__page = src
        self.__soup = BeautifulSoup(self.__page, 'html.parser')

    def get_title(self) -> str:
        """
        Получение заголовка патента
        :return: заголовок патента
        """
        return self.__soup.find('h1', {'id': 'title'}).text.strip()

    def get_assignee(self) -> str:
        """
        Получение правопреемника
        :return: Правопреемник
        """
        return self.__soup.select_one('[data-assignee]')['data-assignee']

    def get_authors(self) -> List[str]:
        """
        получение списка авторов
        :return: Список авторов
        """
        return [data_inventor['data-inventor'] for data_inventor in self.__soup.select('[data-inventor]')]

    def get_publications(self) -> List[str]:
        """
        получение списка дат публикаций
        :return: список дат публикаций
        """
        return [publication_tag.text for publication_tag in self.__soup.find_all('div', {'class': 'publication'})]

    def get_abstract(self) -> str:
        """
        Получение аннотации
        :return: текст аннотации патента
        """
        return self.__soup.find('patent-text', {'name': 'abstract'}).text.strip()

    def get_description(self) -> str:
        """
        получение описания патента
        :return: текст описания патента
        """
        try:
            description = self.__soup.find('patent-text', {'name': 'description'}).text.strip()
        except:
            description = ''
        return description

    def get_description_en(self) -> str:
        """
        Получение текста описания на английском
        (альтернативный вариант)
        :return:
        """
        description = ''
        for x in self.__soup.find_all('description', class_='style-scope patent-text'):
            for y in x.find_all(class_='notranslate style-scope patent-text'):
                for txt in y.find_all(string=True):
                    parent_class = txt.parent['class']
                    if len(txt) == 1:
                        description += '\n'
                        continue
                    if parent_class[0] != 'google-src-text':
                        description += txt
                    else:
                        pass

        return description

    def get_us_patent(self) -> str:
        """
         Получение номера патента на английском и ссылку
        :return: номер патента, ссылка
        """

        links_lng_patent = self.__soup.find_all("state-modifier", class_="style-scope application-timeline")
        link_en = ""
        links_en = []
        patent = ''
        if len(links_lng_patent):
            for link in links_lng_patent:
                if link.text.strip() == "US":
                    link_en = "https://patents.google.com/" + link.get("data-result")
                    patent = link.get("data-result")
                    patent = patent.replace('/en', '')
                    patent = patent.replace('patent/', '')
                    links_en.append(link_en)

        return patent, link_en

    def get_us_patent_link(self) -> str:
        """
        Получение ссылки на английский патент (массив)
        :return: ссылка
        """
        # ссылки(а) на en патенты (доделать)
        links_lng_patent = self.__soup.find_all("state-modifier", class_="style-scope application-timeline")
        link_en = " "
        links_en = []

        if len(links_lng_patent):
            for link in links_lng_patent:
                if link.text.strip() == "US":
                    link_en = "https://patents.google.com/" + link.get("data-result")
                    links_en.append(link_en)

        return link_en
