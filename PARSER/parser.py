import os
import sys
import time
from datetime import datetime
from colorama import init

from DRIVERS.driver import PatentWebDriver
from DRIVERS.patent import GooglePatent
from parse_descr import create_corpus
from DB.db_mysql_driver import MySQLDriver
from DB.db_clickhouse_driver import ClickHouseDriver
from globals import TEMP_PATH, DEBUG_FLAG
import platform
from globals import MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE
from globals import HOUSE_HOST, HOUSE_PORT, HOUSE_USER, HOUSE_PASSWORD, HOUSE_DATABASE

init()
sys.setrecursionlimit(10000)
initial_load = 1


def info_msg(patent, info):
    """
    Текстовое сообщение о ходе обработки
    :param patent: номер патента
    :param info:статус обработки
    :return: выходная строка
    """
    current_datetime = datetime.now()
    time_str = str(current_datetime.hour) + ":" + str(current_datetime.minute) + ":" + str(
        current_datetime.second)
    _msg = '[' + str(datetime.now().date()) + '][' + time_str + '][' + patent + '] ' + info
    return _msg


def main():
    """
     Основная функция
    """
    # Определение текущей OS
    osp = platform.platform()
    # Переключение на СУБД в зависимости от текущей OS
    if osp.startswith('Windows') == 1:
        db_cursor = MySQLDriver(MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE)
    else:
        db_cursor = ClickHouseDriver(HOUSE_HOST, HOUSE_PORT, HOUSE_USER, HOUSE_PASSWORD, HOUSE_DATABASE)
    db_cursor.connect_db()
    # очистка параллельного корпуса
    if initial_load != 0:
        db_cursor.delete_t_texts()
    # Получение списка патентов из бд
    start_time = time.monotonic()
    all_patent_links = db_cursor.get_all_links()
    #  Получение заголовков абзацев
    hdrs = db_cursor.select_t_headers()
    # создание объектов
    driver = PatentWebDriver()
    Patent_ru = GooglePatent(driver)
    Patent_en = GooglePatent(driver)
    # Обработка патентов
    for url in all_patent_links:
        if url[0] != '':
            patent_num = url[0]
            exist = db_cursor.already_t_done(patent_num)
            # если если еще не обрабатывали:
            if not exist:
                # читаем загруженный в БД патент для разбора
                patent = url[0]
                src_ru = db_cursor.patent_get_src(url[0])
                Patent_ru.set_data(src_ru)
                text_ru = Patent_ru.get_description()
                src_en = db_cursor.patent_get_src(url[1])
                if src_en is not None:
                    Patent_en.set_data(src_en)
                    text_en = Patent_en.get_description()
                else:
                    text_en = ''

                # Разбор и сопоставление абзацев по предложениям
                texts = create_corpus(patent, text_en, text_ru, hdrs)

                # Сохранение в БД
                for text in texts:
                    db_cursor.insert_t_texts(text[0], text[1], text[2], text[3], text[4])
                if len(texts) == 0:  # Если не найдено сохраняем пустую строку
                    db_cursor.insert_t_texts(url[0], '0', '', '', 'X')
                _msg = info_msg(url[0], 'данные сохранены в БД')
                print(_msg)
            else:
                _msg = info_msg(url[0], 'уже сохранен в БД')
                print(_msg)
        else:
            _msg = info_msg(url[0], 'еще не переведен')
            print(_msg)
    t = time.monotonic() - start_time
    print('парсинг окончен [' + str(t) + ']')
    driver.quit()


if __name__ == "__main__":
    main()
