from nltk.tokenize import sent_tokenize
import re
from globals import TEMP_PATH, DEBUG_FLAG


def is_header(lin: str):
    """
    Проверка, является лм строка заголовком
    :param lin: строка
    :return: Флаг заголовка либо 1 либо 0
    """
    flag = lin.find('!!!')
    return flag


def find_paragraf_by_hdr(hdr, lines):
    """
    Поиск абзаца по заголовкам
    :param hdr: Заголовок
    :param lines: исходные строки
    :return: позиция начала и конца абзаца для заголовка
    """
    arraylen = len(lines)
    r = range(0, arraylen)
    start = -1
    end = len(lines)
    for i in r:
        line = lines[i]
        line = line.upper()
        if (is_header(line)) > 0:
            if line == hdr:
                start = i
                break

    if start >= 0:
        r = range(start + 1, arraylen)
        for i in r:
            line = lines[i]
            if is_header(line) > 0:
                end = i
                break

    if start == -1: start = end
    return start, end


def create_corpus(patent, descr_en, descr_ru, hdr):
    """
    Построение параллельного корпуса
    :param patent: номер патента
    :param descr_en: описание на английском языке
    :param descr_ru: описание на русском языке
    :param hdr: таблица заголовков
    :return: массив распараллеленного текста
    """
    # 1 раз запускаем для скачивания    nltk.download('punkt')
    # удаление квадр скобок [....]
    descr_en = re.sub('\[\d*\]', '', descr_en)  # del [0001]
    descr_ru = re.sub('\[\d*\]', '', descr_ru)  # del [0001]
    # Удаление цифр из текста
    descr_en = re.sub('\d*', '', descr_en)  # del цифры
    descr_ru = re.sub('\d*', '', descr_ru)  # del
    # Удаление пустых строк
    descr_en = descr_en.replace(' \n \n', '')
    descr_en = descr_en.replace(' \n', '')
    descr_ru = descr_ru.replace(' \n \n', '')
    descr_ru = descr_ru.replace(' \n', '')
    # удаление хитрого перевода строки
    descr_en = descr_en.replace(';\n', '????')
    descr_en = descr_en.replace('????', '.')

    # удаление хитрого перевода строки
    descr_ru = descr_ru.replace(';\n', '????')
    descr_ru = descr_ru.replace('????', '.')

    # !!! признак заголовка
    descr_en = descr_en.replace('\n', '!!! ')
    descr_en = descr_en.replace('.!!!', '.')
    descr_en = descr_en.replace('!\n', ' ')
    descr_en = descr_en.replace('!!\n', ' ')

    # удаление точек из сокращений замена на синонимы
    descr_en = descr_en.replace('i.e.', 'ie')
    descr_en = descr_en.replace('e.g.', 'for example')
    descr_en = descr_en.replace('etc..,', '')
    descr_en = descr_en.replace('etc.', 'etc')
    descr_en = descr_en.replace('FIG.', 'figure')
    descr_en = descr_en.replace('FIGS.', 'figures')
    descr_en = descr_en.replace('Pat.No.', 'Patent')
    descr_en = descr_en.replace('No.', 'Number')

    descr_ru = descr_ru.replace('Фиг.', 'рисунок')
    descr_ru = descr_ru.replace('фиг.', 'рисунок')
    descr_ru = descr_ru.replace('!\n', ' ')
    descr_ru = descr_ru.replace('!!\n', ' ')

    descr_ru = descr_ru.replace('\n', '!!! ')
    descr_ru = descr_ru.replace('.!!!', '.')
    descr_ru = descr_ru.replace('т.е.', '')
    descr_ru = descr_ru.replace('к.п.д.', 'коэффициент полезного действия')
    descr_ru = descr_ru.replace('и т.д.', '')
    descr_ru = descr_ru.replace('п.', '')

    #   Разбитие на предложения
    en = sent_tokenize(descr_en)
    ru = sent_tokenize(descr_ru, 'russian')

    texts = []
    ru = list(filter(lambda x: len(x) > 10, ru))
    en = list(filter(lambda x: len(x) > 10, en))

    posnr = 0  #
    # цикл: разбитие на абзацы и вывод предложения в одну строку
    for hdr_l in hdr:
        # поиск параграфов
        startru, endru = find_paragraf_by_hdr(hdr_l[2], ru)
        starten, enden = find_paragraf_by_hdr(hdr_l[1], en)
        lenp_en = enden - starten
        lenp_ru = endru - startru
        # Если ничего не найдено
        if lenp_en < 2 or lenp_ru < 2:
            continue
        k = startru  # тек позиции
        l = starten
        flagparcopied = False
        # Пока не конец абзаца копируем предложения в корпус
        while not flagparcopied:
            if l < enden:
                str_en = en[l]
            else:
                str_en = ''
            l = l + 1

            if k < endru:
                str_ru = ru[k]
            else:
                str_ru = ''

            if k == startru:
                str_ru = str_ru.upper()
                hdr_flag = 'X'
            else:
                hdr_flag = ''
            k = k + 1
            # удаление пробелов в начале и конце строки и символов переноса строки
            str_en = re.sub("^\s+|\n|\r|\s+$", '', str_en)
            str_ru = re.sub("^\s+|\n|\r|\s+$", '', str_ru)
            if str_ru == '' and str_en == '':
                flagparcopied = True
            else:
                text_s = patent + '\t' + str_en + '\t' + str_ru + '\t' + hdr_flag + '\n'
                text = [patent, posnr, str_en, str_ru, hdr_flag]
                texts.append(text)
                posnr = posnr + 1
    return texts
