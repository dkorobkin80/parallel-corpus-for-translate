from DB.db_interface import DBInterface
from clickhouse_driver import Client
import pandas as pd


# Класс подключения к СУБД ClickHouse
class ClickHouseDriver(DBInterface):
    def __init__(self, host, port, user, password, database) -> None:
        """
               Конструктор класса
               :param host: адрес подключения
               :param port: порт подключения
               :param user: логин пользователя
               :param password: пароль пользователя
               :param database: название БД
               """
        self._client = ''
        self._host = host
        self._port = port
        self._user = user
        self._password = password
        self._database = database

    # подключение к базе данных
    def connect_db(self) -> None:
        """
               Подключение к БД
               :return:
        """
        print("подключение к базе данных ", end='')
        try:
            self._client = Client(self._host,
                                  user=self._user,
                                  password=self._password,
                                  secure=False,
                                  verify=False,
                                  database=self._database,
                                  compression=False,
                                  settings={'use_numpy': True})
            result = self._client.execute('SELECT now(), version()')
            # print("RESULT: {0}: {1}".format(type(result), result))
            print("[успешно]")

        except Exception as ex:
            print("[безуспешно]")
            print(ex)

    def insert_t_texts(self, patent, posnr, text_en, text_ru, hdr_flg):
        """
            Запись новой строки в таблицу корпуса
            :param patent: номер патента.
            :param posnr: cчётчик.
            :param text_en: текст на английском.
            :param text_ru: текст на русском.
            :param hdr_flg: флаг заголовка.
        """
        text_en = text_en.replace("\'", "\\'").replace("`", "\\'")
        text_ru = text_ru.replace("\'", "\\'").replace("`", "\\'")
        posnum = str(posnr)
        # Creating Dataframe
        df = pd.DataFrame([
            [patent, posnum, text_en, text_ru, hdr_flg]
        ],
            columns=['patent_num', 'posnr', 'text_en', 'text_ru', 'hdr_flg'])

        self._client.insert_dataframe(f'INSERT INTO patents.texts VALUES', df)

    def delete_t_texts(self):
        """
            Удаление данных таблицы корпуса.
        """
        delete_query = "TRUNCATE TABLE patents.texts"
        result = self._client.execute(delete_query)

    def select_t_headers(self) -> list:
        """
            Получение списка заголовков.
            :return: список заголовков.
        """
        query = "SELECT * FROM `headers`"
        return self._client.execute(query)

    def get_all_links(self):
        """
                получение списка ссылок на патенты.
                :return: список ссылок на патенты для парсинга.
        """
        select_query = "SELECT * FROM `links` WHERE link_en  <> " + "\'\'"
        all_patent_links = self._client.execute(select_query)
        return all_patent_links

    def already_t_done(self, patent):
        """
               Проверка наличия патента в параллельном корпусе.
               :param patent: номер патента.
               :return: результат проверки.
        """
        select_query = "SELECT patent_num FROM `texts` WHERE patent_num = " + "\'" + patent + "\'"
        exist = self._client.execute(select_query)
        return exist

    def get_link(self, patent):
        """
                Получение номера англ патента для рускоязычного патента.
                :param patent: номер патента.
                :return: номер английского патента.
        """
        select_query = "SELECT patent_en FROM `links` WHERE patent_ru = " + "\'" + patent + "\'"
        self._client.execute(select_query)
        exist = self._client.fetchone()
        return exist['patent_en']

    def get_texts(self):
        """
               Выбор параллельного корпуса.
               для дальнейшей обработки в модели обучения.
               :return: параллельный корпус.
        """
        select_query = "SELECT * FROM `texts` WHERE  hdr_flg = " + "\'" + "\'"
        all_texts = self._client.execute(select_query)

        return all_texts

    def patent_get_src(self, patent):
        """
                Выбор HTML страницы патента.
                :param patent: номер патента.
                :return: html страница.
        """
        select_query = "SELECT html FROM `pat_html` WHERE patent = " + "\'" + patent + "\'"
        src = self._client.execute(select_query)

        return src[0][0]

    def insert_base(self, patent, patent_en, link_en, link_ru):
        """
                Запиьсь данных в таблицу links.
                :param patent: номер патента на русском ячзыке.
                :param patent_en: номер патента на английском языке.
                :param link_en: ссылка на англ.патент.
                :param link_ru: ссылка на русский патент.
                """
        df = pd.DataFrame([
            [patent, patent_en, link_en, link_ru, '', '']
        ],
            columns=['patent_ru', 'patent_en', 'link_en', 'link_ru', 'processed', 'bad'])

        self._client.insert_dataframe(f'INSERT INTO patents.links VALUES', df)

    def insert_html(self, patent, src):
        """
                Сохранение html страницы патента.
                :param patent: номер патента.
                :param src: html страница.
        """
        df = pd.DataFrame([
            [patent, src]
        ],
            columns=['patent', 'html'])

        self._client.insert_dataframe(f'INSERT INTO patents.pat_html VALUES', df)

    def delete_all_from_db(self):
        """
                Очистка всех таблиц.
        """
        delete_query = "TRUNCATE `links`"
        self._client.execute(delete_query)
        delete_query = "TRUNCATE `pat_html`"
        self._client.execute(delete_query)
        delete_query = "TRUNCATE `texts`"
        self._client.execute(delete_query)

        pass

    def is_patent_exists(self, patent_num):
        """
                Проверка что патент уже обрабатывался(поиск патентов).
                :param patent_num: номер патента.
                :return: запись таблицы.
        """
        select_query = "SELECT patent_ru FROM `links` WHERE patent_ru = " + "\'" + patent_num + "\'"
        exist = self._client.execute(select_query)
        return exist
