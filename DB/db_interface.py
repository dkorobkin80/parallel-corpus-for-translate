from abc import ABC, abstractmethod

# Класс управления подключения к БД
class DBInterface(ABC):

    @abstractmethod
    def connect_db(self) -> None:
        """
               Подключение к БД
               :return:
        """
        pass

    # вставить в базу патента
    @abstractmethod
    def insert_t_texts(self, patent, posnr, text_en, text_ru, hdr_flg):
        """
            Запись новой строки в таблицу корпуса
            :param patent: номер патента.
            :param posnr: cчётчик.
            :param text_en: текст на английском.
            :param text_ru: текст на русском.
            :param hdr_flg: флаг заголовка.
        """
        pass

    @abstractmethod
    def delete_t_texts(self):
        """
                    Удаление данных таблицы корпуса.
        """
        pass

    @abstractmethod
    def select_t_headers(self) -> list:
        """
                    Получение списка заголовков.
                    :return: список заголовков.
        """
        pass

    @abstractmethod
    def get_all_links(self):
        """
                получение списка ссылок на патенты.
                :return: список ссылок на патенты для парсинга.
        """
        pass

    @abstractmethod
    def already_t_done(self, patent):
        """
                Проверка наличия патента в параллельном корпусе.
                :param patent: номер патента.
                :return: результат проверки.
        """
        pass

    @abstractmethod
    def get_link(self, patent):
        """
                Получение номера англ патента для рускоязычного патента.
                :param patent: номер патента.
                :return: номер английского патента.
        """
        pass

    @abstractmethod
    def get_texts(self):
        """
                Выбор параллельного корпуса.
                для дальнейшей обработки в модели обучения.
                :return: параллельный корпус.
        """
        pass

    @abstractmethod
    def patent_get_src(self, patent):
        """
                Выбор HTML страницы патента.
                :param patent: номер патента.
                :return: html страница.
        """
        pass

    @abstractmethod
    def insert_base(self, patent, patent_en, link_en, link_ru):
        """
                Запиьсь данных в таблицу links.
                :param patent: номер патента на русском ячзыке.
                :param patent_en: номер патента на английском языке.
                :param link_en: ссылка на англ.патент.
                :param link_ru: ссылка на русский патент.
        """
        pass

    @abstractmethod
    def insert_html(self, patent, src1):
        """
                Сохранение html страницы патента.
                :param patent: номер патента.
                :param src: html страница.
        """
        pass

    @abstractmethod
    def delete_all_from_db(self):
        """
               Очистка всех таблиц.
        """
        pass

    @abstractmethod
    def is_patent_exists(self, patent_num):
        """
                Проверка что патент уже обрабатывался(поиск патентов).
                :param patent_num: номер патента.
                :return: запись таблицы.
        """
        pass
