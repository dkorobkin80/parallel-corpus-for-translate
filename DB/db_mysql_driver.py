from DB.db_interface import DBInterface
import pymysql


# Класс подключения к СУБД MySQL
class MySQLDriver(DBInterface):
    def __init__(self, host, port, user, password, database) -> None:
        """
        Конструктор класса
        :param host: адрес подключения
        :param port: порт подключения
        :param user: логин пользователя
        :param password: пароль пользователя
        :param database: название БД
        """
        self._connection = ''
        self._cursor = ''
        self._host = host
        self._port = port
        self._user = user
        self._password = password
        self._database = database

    def connect_db(self) -> None:
        """
        Подключение к БД
        :return:
        """
        print("подключение к базе данных ", end='')
        try:

            self._connection = pymysql.connect(
                host=self._host,
                port=self._port,
                user=self._user,
                password=self._password,
                database=self._database,
                cursorclass=pymysql.cursors.DictCursor
            )
            self._cursor = self._connection.cursor()
            print("[успешно]")
        except Exception as ex:
            print("[безуспешно]")
            print(ex)


    def insert_t_texts(self, patent, posnr, text_en, text_ru, hdr_flg):
        """
            Запись новой строки в таблицу корпуса
            :param patent: номер патента.
            :param posnr: cчётчик.
            :param text_en: текст на английском.
            :param text_ru: текст на русском.
            :param hdr_flg: флаг заголовка.
        """
        text_en = text_en.replace("\'", "\\'").replace("`", "\\'")
        text_ru = text_ru.replace("\'", "\\'").replace("`", "\\'")
        posnr = str(posnr)
        insert_query = "INSERT INTO `texts` (patent_num, posnr, text_en, text_ru,hdr_flg) VALUES (" + "\'" + patent + "\'" + "," + "\'" + posnr + "\'" + "," + "\'" + text_en + "\'" + "," + "\'" + text_ru + "\'" + "," + "\'" + hdr_flg + "\'" + ")" + ";"
        self._cursor.execute(insert_query)
        self._connection.commit()

    def delete_t_texts(self):
        """
            Удаление данных таблицы корпуса.
        """
        delete_query = "DELETE FROM `texts`"
        self._cursor.execute(delete_query)
        self._connection.commit()

    def select_t_headers(self) -> list:
        """
            Получение списка заголовков.
            :return: список заголовков.
        """
        arr_list = []
        query = "SELECT * FROM `headers`"
        self._cursor.execute(query)
        hdrs = self._cursor.fetchall()
        self._connection.commit()
        for dta in hdrs:
            arr = [dta['sort_order'], dta['hdr_en'], dta['hdr_ru']]
            arr_list.append(arr)
        return arr_list

    def get_all_links(self):
        """
        получение списка ссылок на патенты.
        :return: список ссылок на патенты для парсинга.
        """
        arr_list = []
        select_query = "SELECT * FROM `links` WHERE link_en  <> " + "\'" + "" + "\'"
        self._cursor.execute(select_query)
        all_patent_links = self._cursor.fetchall()
        for dta in all_patent_links:
            arr = [dta['patent_ru'], dta['patent_en'], dta['link_en'], dta['link_ru']]
            arr_list.append(arr)
        return arr_list

    def already_t_done(self, patent):
        """
        Проверка наличия патента в параллельном корпусе.
        :param patent: номер патента.
        :return: результат проверки.
        """
        select_query = "SELECT patent_num FROM `texts` WHERE patent_num = " + "\'" + patent + "\'"
        self._cursor.execute(select_query)
        exist = self._cursor.fetchone()
        return exist

    def get_link(self, patent):
        """
        Получение номера англ патента для рускоязычного патента.
        :param patent: номер патента.
        :return: номер английского патента.
        """
        select_query = "SELECT patent_en FROM `links` WHERE patent_ru = " + "\'" + patent + "\'"
        self._cursor.execute(select_query)
        exist = self._cursor.fetchone()
        return exist['patent_en']

    def get_texts(self):
        """
        Выбор параллельного корпуса.
        для дальнейшей обработки в модели обучения.
        :return: параллельный корпус.
        """
        arr_list = []
        select_query = "SELECT * FROM `texts` WHERE  hdr_flg = " + "\'" + "\'"
        all_texts = self._cursor.execute(select_query)
        all_texts = self._cursor.fetchall()
        for dta in all_texts:
            arr = [dta['patent_num'], dta['posnr'], dta['text_en'], dta['text_ru']]
            arr_list.append(arr)
        return arr_list

    def patent_get_src(self, patent):
        """
        Выбор HTML страницы патента.
        :param patent: номер патента.
        :return: html страница.
        """
        select_query = "SELECT html FROM `pat_html` WHERE patent = " + "\'" + patent + "\'"
        self._cursor.execute(select_query)
        src = self._cursor.fetchone()
        if src is None:
            html = ''
        else:
            html = src['html']

        return html

    def insert_base(self, patent, patent_en, link_en, link_ru):
        """
        Запиьсь данных в таблицу links.
        :param patent: номер патента на русском ячзыке.
        :param patent_en: номер патента на английском языке.
        :param link_en: ссылка на англ.патент.
        :param link_ru: ссылка на русский патент.
        """
        insert_query = "INSERT INTO `links` (patent_ru,patent_en,link_en, link_ru) VALUES (" + "\'" + patent + "\'" + "," + \
                       "\'" + patent_en + "\'" + "," + "\'" + link_en + "\'" + "," + "\'" + link_ru + "\'" + ")" + \
                       " ON DUPLICATE KEY UPDATE link_en= " + "\'" + link_en + "\'" ";"
        self._cursor.execute(insert_query)
        self._connection.commit()

    def insert_html(self, patent, src):
        """
        Сохранение html страницы патента.
        :param patent: номер патента.
        :param src: html страница.
        """

        insert_query = "INSERT INTO `pat_html` (patent,html) VALUES (" + "\'" + patent + "\'" + "," + \
                       "\'" + self._connection.escape_string(str(src)) + "\'" + ")" + \
                       " ON DUPLICATE KEY UPDATE patent = " + "\'" + patent + "\'" ";"
        self._cursor.execute(insert_query)
        self._connection.commit()

    def delete_all_from_db(self):
        """
        Очистка всех таблиц.
        """
        delete_query = "DELETE FROM `links`"
        self._cursor.execute(delete_query)
        self._connection.commit()
        delete_query = "DELETE FROM `texts`"
        self._cursor.execute(delete_query)
        self._connection.commit()
        delete_query = "DELETE FROM `pat_html`"
        self._cursor.execute(delete_query)
        self._connection.commit()

    def is_patent_exists(self, patent_num):
        """
        Проверка что патент уже обрабатывался(поиск патентов).
        :param patent_num: номер патента.
        :return: запись таблицы.
        """
        select_query = "SELECT patent_ru FROM `links` WHERE patent_ru = " + "\'" + patent_num + "\'"
        return self._cursor.execute(select_query)
