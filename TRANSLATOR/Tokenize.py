import spacy
import re


# токенайзер
class tokenize(object):

    def __init__(self, lang):
        """
        инициализация
        :param lang: код языка
        """
        self.nlp = spacy.load(lang)

    def tokenizer(self, sentence):
        """
        Разбитие предложения на слова
        удаляя лишние символы
        :param sentence:
        :return:
        """
        sentence = re.sub(
            r"[\*\"“”\n\\…\+\-\/\=\(\)‘•:\[\]\|’\!;]", " ", str(sentence))
        sentence = re.sub(r"[ ]+", " ", sentence)
        sentence = re.sub(r"\!+", "!", sentence)
        sentence = re.sub(r"\,+", ",", sentence)
        sentence = re.sub(r"\?", "", sentence)
        sentence = re.sub(r"\,", "", sentence)
        sentence = sentence.lower()

        return [tok.text for tok in self.nlp.tokenizer(sentence) if tok.text != " "]
