from tkinter import *
from tkinter import ttk, messagebox
from translate import get_translated
from translate import init_translat

root = Tk()
root.title('Перевести')
root.geometry("1000x600")


def translate_it():
    """
    обработка кнопки "перевести"
    :return:
    """
    tr_text = original_text.get(1.0, END).strip().lower()
    if not tr_text:
        messagebox.showerror("Внимание", "Введите текст для перевода.")
        return
    # перевод текста из окна
    txt = get_translated(opt, model, SRC, TRG, tr_text)
    translated_text.insert(1.0, txt)


def clear():
    """
    очистка окон
    """
    original_text.delete(1.0, END)
    translated_text.delete(1.0, END)


original_text = Text(root, height=20, width=50)
original_text.grid(row=0, column=0, pady=20, padx=10)
translated_text = Text(root, height=20, width=50)
translated_text.grid(row=0, column=2, pady=20, padx=10)
# Определяем кнопку "Перевести"
translate_button = Button(
    root,
    text="Перевести",
    font=("Helvetica", 14),
    command=translate_it
)
translate_button.grid(row=0, column=1, padx=10)

# Определяем кнопку "Очистить"
clear_button = Button(root, text="Очистить", command=clear)
clear_button.grid(row=2, column=1)
SRC, TRG, model, opt = init_translat()

root.mainloop()
